const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

dotenv.config()

const app = express()
const port = 7007
// for heroku deployment
// const port = process.env.port || 7007

// MongoDB Connection
mongoose.connect(`mongodb+srv://kenheilzer:${process.env.MONGODB_PASSWORD}@cluster0.wtjmdvw.mongodb.net/Capstone-3:e-commerce?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.once('open', () => console.log('Connected to MongoDB!'))

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// ROUTES >>>>>>

app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)

// ROUTES END >>>>>>

app.listen(process.env.PORT || 7007, () => {
	console.log(`API is now running on localhost: ${process.env.PORT || 7007}`)
})