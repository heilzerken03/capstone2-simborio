const Product = require('../models/Product')
const User = require('../models/User')
const Order = require('../models/Order')
const auth = require('../auth')

// Create Product (Admin only)
module.exports.createProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product({
			prodName: data.product.prodName,
			description: data.product.description,
			price: data.product.price
		})

		return new_product.save().then((created_product, error) => {
			if(error){
				return false
			}

			return {
				message: 'New product successfully created!'
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be an ADMIN to create a new product.'
	})

	return message.then((value) => {
		return value
	})
}

// Retrieve all active products
module.exports.getAllProducts = (data) => {
	return Product.find().then((result) => {
		return result
	})
}

// Retrieve single product
module.exports.getSingleProducts = (product_id) => {
	return Product.findById(product_id).then((result) => {
		return result
	})
}

// Update Product information (Admin only)
module.exports.updateProduct = (product_id, new_data) => {
	return Product.findByIdAndUpdate(product_id, {
		prodName: new_data.prodName,
		description: new_data.description,
		price: new_data.price
	}).then((updated_product, error) => {
		if(error){
			return false
		}

		return {
			message: "Product information has been successfully updated!"
		}
	})
}

// Archive Product (Admin only)
module.exports.archiveProduct = (product_id) => {
	return Product.findByIdAndUpdate(product_id, {
		isActive: false
	}).then((archived_product, error) => {
		if(error){
			return false
		}

		return {
			message: "Product has been successfully archived!"
		}
	})
}

// Enable Product (Admin only)
module.exports.enableProduct = (product_id) => {
	return Product.findByIdAndUpdate(product_id, {
		isActive: true
	}).then((archived_product, error) => {
		if(error){
			return false
		}

		return {
			message: "Product has been successfully archived!"
		}
	})
}