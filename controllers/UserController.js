const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Check if email exists
module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0) {
			return true
		}
		return false
	})
}

// User Registration
module.exports.register = (data) => {
    	return User.find({email: data.email}).then((result) => {
		if(result.length > 0) {
			return {
				message: "Email already used! Please try another email."
			}
		}
   // if(data.firstName === " " || data.lastName === " " || data.mobileNo === " " || data.email === " "){
   //  	return Promise.resolve({
   //  		message: "Please provide the required input!"
   //  	})
   //  }
	let encrypted_password = bcrypt.hashSync(data.password,10)
	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		mobileNo: data.mobileNo,
        email: data.email,
        password: encrypted_password,
    })

    return new_user.save().then((created_user,error) => {
        if(error){
            return false	
        	}
        		return {
           	 		message: "User successfully registered"
        		}
    	}) 
	})
}

	

// User authentication
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User does not exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)
		if(is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}
	})
}

// Retrieve user details
module.exports.retrieveUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((result) => {
		return result
	})
}

// Get User Details
module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        // Makes the password not be included in the result
        result.password = "";
        // Returns the user information with the password as an empty string
        return result;
    });
};

// Set user as admin (Admin only access)
module.exports.setAsAdmin = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password,10)

	let  new_admin = new User({
        email: data.email,
        password: encrypted_password,
        isAdmin: true
    })

    return new_admin.save().then((admin_access,error) => {
        if(error){
            return false
        }

        return {
            message: 'User successfully registered as an Admin!' 
        }
    })
}