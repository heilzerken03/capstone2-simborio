const Order = require('../models/Order')
const Product = require('../models/Product')
const User = require('../models/User')
const auth = require('../auth')

// Non-admin User checkout (Create Order)
module.exports.createOrder = (data) => {
		let new_order = new Order({
			userId: data.userId,
			prodName: data.prodName,
			products: {
				productId: data.productId,
				quantity: data.quantity
			},
			totalAmount: data.totalAmount
		})

		return new_order.save().then((created_order, error) => {
			if(error){
				return false
			}

			return {
				message: 'Your order has been successfully created!'
			}
		})
	}	

// Order Product
	module.exports.orderProduct = async (data) => {
	    if(data.isAdmin){
	        let message = Promise.resolve({
	            message: "Admin users can't access this feature."
	        })
	        return message.then((result) => result)
	    }
	    // Responsible for storing product info to be extracted for total computation
	    let productInfo = await Product.findById(data.info.productId).then((productDetails) => productDetails)
	    let new_order = new Order({
	        userId: data.info.userId,
	        prodName: data.info.prodName,
	        products: {
	            productId: data.info.productId,
	            quantity: data.info.quantity
	        },
	        totalAmount: productInfo.price*data.info.quantity
	    })
	    return new_order.save().then((order, error) => {
	        if(error){
	            return false
	        }
	        return {
	            message: 'Order successful!'
	        }
	    })
	};

// Retrieve all orders (Admin only)
module.exports.getAllOrders = (data) => {
	return Order.find({}).then((result) => {
		return result
	})
}

// Retrieve authenticated user’s orders
module.exports.getUserOrders = (data) => {
	return Order.find({userId: data}).then((result) => {
		return result
	})
}
