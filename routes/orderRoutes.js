const express = require('express')
const router = express.Router()
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')

// Non-admin User checkout (Create Order)
router.post("/create-order", auth.verify, (request, response) => {
	const data = {
		userId: request.body.userId,
		prodName: request.body.prodName,
		productId: request.body.productId,
		quantity: request.body.quantity,
		totalAmount: request.body.totalAmount

		// isAdmin: auth.decode(request.headers.authorization).isAdmin
		// userId: auth.decode(request.headers.authorization).Id
	}

	OrderController.createOrder(data).then((result) => {
		response.send(result)
	})
})

// Order Product
router.post("/order-product", auth.verify, (request, response) => {
    let data = {
        info: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    OrderController.orderProduct(data).then((result) => {
        response.send(result);
    });
});

// Retrieve all orders (Admin only)
router.get("/all-orders", auth.verify, (request, response) => {
	OrderController.getAllOrders().then((result) => {
		response.send(result)
	})
})

// Retrieve authenticated user’s orders
router.get("/:userId/user-orders", auth.verify, (request, response) => {
	OrderController.getUserOrders(request.params.userId).then((result) => {
		response.send(result)
	})
})


module.exports = router