const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// Check if email exists
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})

// User registration
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// User authentication
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Retrieve user details
router.get("/:id/details", auth.verify, (request, response) => {
	UserController.retrieveUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// Get user details from token
router.get("/details", auth.verify, (request, response) => {
    const user_data = auth.decode(request.headers.authorization);
    UserController.getProfile({userId : user_data.id}).then(result => response.send(result));
});

// Set user as admin (Admin only access)

router.post("/admin", auth.verify, (request, response) => {
	UserController.setAsAdmin(request.body).then((result) => {
		response.send(result)
	})
})

module.exports = router