const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// Create Product (Admin only)
router.post("/create-product", auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.createProduct(data).then((result) => {
		response.send(result)
	})
})

// Retrieve all active products
router.get("/", (request, response) => {
	ProductController.getAllProducts().then((result) => {
		response.send(result)
	})
})

// Retrieve single product
router.get("/:productId", (request, response) => {
	ProductController.getSingleProducts(request.params.productId).then((result) => {
		response.send(result)
	})
})

// Update Product information (Admin only)
router.patch("/:productId/update", auth.verify, (request, response) => {
	ProductController.updateProduct(request.params.productId, request.body).then((result) => {
		response.send(result)
	})
})

// Archive Product (Admin only)
router.patch("/:productId/archive", auth.verify, (request, response) => {
	ProductController.archiveProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

// Enable Product (Admin only)
router.patch("/:productId/enable", auth.verify, (request, response) => {
	ProductController.enableProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})


module.exports = router